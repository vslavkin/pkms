:PROPERTIES:
:ID:       d3485adc-d5db-464b-ae9f-e4e0818e5029
:END:
#+title: Binary tree
#+filetags: :TODO:What:
* What is a Binary tree?
A binary tree is a [[id:d48451bf-3208-4823-af59-233dc0fe592c][Data strucutre]] in which each node has at most *two* children.
In [[id:73cfa63b-5598-440d-8ffa-21dfa356457e][Graph Theory]], it's a directed, rooted [[id:657575a4-4690-4a32-945b-736d553d1961][Tree]] with at most two children per vertex.
* Common uses
They are usually used to efficiently save data that will be searched and sorted frequently.
* Reference  
[[https://en.wikipedia.org/wiki/Binary_tree][Wikipedia]]
