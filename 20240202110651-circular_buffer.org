:PROPERTIES:
:ID:       9f74b077-64d3-4d12-9cf7-5de246936c5c
:END:
#+title: Circular Buffer
#+filetags: :TODO:Algorithm:What:
* What is a Circular Buffer?

#+DOWNLOADED: https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Circular_Buffer_Animation.gif/400px-Circular_Buffer_Animation.gif @ 2024-02-02 11:07:59
[[file:media/What_is_Circular_Buffer?/2024-02-02_11-07-59_400px-Circular_Buffer_Animation.gif]]
* Reference  
[[https://en.wikipedia.org/wiki/Circular_buffer][Wikipedia]]
