:PROPERTIES:
:ID:       404ac42a-2f3f-4129-8917-f65306f9074f
:END:
#+title: Triac
#+filetags: :Electronics:TODO:What:

* What is an Triac?
[[id:47412c5a-14b3-41ad-b775-46cb7b13ff2d][Semiconductor Device]]
Triac = triode for alternating current
It's basically a transistor that can conduct current to both sides

#+DOWNLOADED: https://upload.wikimedia.org/wikipedia/commons/e/e9/TRIAC_Equivalent_Circuit.png @ 2024-01-09 12:03:59
[[file:media/What_is_an_Triac?/2024-01-09_12-03-59_TRIAC_Equivalent_Circuit.png]]

* Reference  
[[https://en.wikipedia.org/wiki/TRIAC][Wikipedia]]
