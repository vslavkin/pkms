:PROPERTIES:
:ID:       1ec93ebd-5a2a-4340-bb2a-232aec73951f
:END:
#+title: notcurses
#+filetags: What
* What is notcurses?
Notcurses is an alternative to the old [[id:c0e89477-1b70-4719-8bf8-ad49804096b3][ncurses]] to make text-based user interfaces.
It has way more features, and is designed to work with threads.
* Reference  
[[https://github.com/dankamongmen/notcurses][Github]]
