:PROPERTIES:
:ID:       f9595e55-4f4b-43e8-bea6-ace23ea9b95d
:END:
#+title: Lisp
#+filetags: :TODO:
* Characteristics

- Family: Lisp
- Inspired by:

  It's the second oldest programming language family still in use.
  It's name means LISt Processor.
  It first appeared in the 1960s and was one of the most innovative languages, introducing concepts as [[id:17a901ab-e964-4d25-8323-6eb2a5fe8947][Automatic dynamic memory management]], 
# TODO   

* Reference:
[[https://en.wikipedia.org/wiki/Lisp_(programming_language)][Lisp (programming language) - Wikipedia]]
