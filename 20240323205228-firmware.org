:PROPERTIES:
:ID:       2e73dc65-6643-4178-8ba6-98bf0ea3c130
:END:
#+title: Firmware
#+filetags: What
* What is Firmware?
A class of [[id:407b765b-8620-4e50-be6f-f9af1d6dbfcb][Computer]] [[id:5b63cb1b-868b-4c7e-92f3-f295fbc39a66][Software]] provides low-level control for the [[id:eace5f73-b2e2-4feb-9890-ba0d3374a4ee][Hardware]].
It's named as an intermediary term between [[id:eace5f73-b2e2-4feb-9890-ba0d3374a4ee][Hardware]] and [[id:5b63cb1b-868b-4c7e-92f3-f295fbc39a66][Software]].
It's usually the only [[id:5b63cb1b-868b-4c7e-92f3-f295fbc39a66][Software]] that simple and limited [[id:eace5f73-b2e2-4feb-9890-ba0d3374a4ee][Hardware]] has.
* Reference  
[[https://en.wikipedia.org/wiki/Firmware][Wikpedia]]
