:PROPERTIES:
:ID:       84e593ed-8077-4e4d-9ab2-30795bfc5e74
:END:
#+title: Electric current
#+filetags: What
* What is Electric current?
Current is a flow of charged particles (usually electrons) moving through a space.
Defined as:
Net rate of flow [[id:0da1151f-30f3-4e0d-9643-0b4664ece9cf][electric charge]] through a surface.

The SI unit for current is the Ampere (amp, $A$). It's equivalent to one Coulomb per second.
* Reference  
[[https://en.wikipedia.org/wiki/Electric_current][Wikipedia]]
