:PROPERTIES:
:ID:       8e3a0463-a163-4cbc-8996-1bcc2443e12c
:END:
#+title: Limits
#+filetags: :Math:What:
* What is a Limit?
A limit is the value that a function approaches as the input approaches some value.
It's usually written as: \[\lim_{x\to c} f(x)=L,\]
And it's read as: "the limit of $f$ of $x$ as $x$ approaches $c$ equals $L$".
That means that the value of the function $f$ can be arbitrarily close to $L$, by choosing $x$ sufficiently close to $c$
Alternatively, the fact that a function f approaches the limit L as x approaches c is sometimes denoted by a right arrow → or \(\displaystyle \rightarrow \), as in
\(f(x) \to L \text{ as } x \to c,\)
Which reads " $f$ of $x$ tends to $L$ as $x$ tends to $c$". 
* Explanation
Imagine a function $f(x) = x+2$.
As $x$ gets closer to 3, $y$ gets closer to 5:

* Reference  
[[https://en.wikipedia.org/wiki/Limit_(mathematics)][Wikipedia]]
[[https://www.khanacademy.org/math/calculus-1/cs1-limits-and-continuity/cs1-limits-intro/a/limits-intro][Khan Academy]]
