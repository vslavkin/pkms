:PROPERTIES:
:ID:       bb9276ae-af76-40be-93d0-e1349253b783
:END:
#+title: Instruction set
#+filetags: What
#+date: [2025-01-17 vie 13:47]
* What is the instruction set?
The instruction set of a [[id:407b765b-8620-4e50-be6f-f9af1d6dbfcb][Computer]] is a model that defines how [[id:5b63cb1b-868b-4c7e-92f3-f295fbc39a66][Software]] controls the [[id:862d0a78-8c28-4be0-b8fa-ab6cf000b11e][CPU]].
Saying it the other way around, the [[id:862d0a78-8c28-4be0-b8fa-ab6cf000b11e][CPU]] is a device that implements the [[id:bb9276ae-af76-40be-93d0-e1349253b783][Instruction set]].

The instruction set is independent of the aforementioned implementation, so several implementations of the same instruction set may have very different internal design and performance.
* Reference  
[[https://en.wikipedia.org/wiki/Instruction_set_architecture][Instruction set architecture - Wikipedia]]
